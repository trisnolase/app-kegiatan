<?php

namespace Tests\Feature\Verifikator;

use App\Models\KegiatanModel;
use Tests\TestCase;

class KegiatanTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        KegiatanModel::factory()->count(1)->create();
    } // function setUp

    public function test_kegiatan_index(): void
    {
        $response = $this->get(route('kegiatan.index'));
        $response->assertSuccessful();
    } // function test_kegiatan_index

    public function test_kegiatan_create(): void
    {
        $response = $this->get(route('kegiatan.create'));
        $response->assertSuccessful();
        $response->assertSee(route('kegiatan.create_post'), false);
    } // function test_kegiatan_create

    public function test_kegiatan_create_post(): void
    {
        $response = $this->post(route('kegiatan.create_post'), [
            'tanggal' => '20-06-2024',
            'deskripsi' => 'klasfhkasf aslkfhljaskh fjasfh',
            'created_by' => 1,
            'modified_by' => 1,
        ]);

        $this->assertDatabaseHas('kegiatan', [
            'tanggal' => '2024-06-20 00:00:00',
            'deskripsi' => 'klasfhkasf aslkfhljaskh fjasfh',
            'created_by' => 1,
            'modified_by' => 1,
        ]);

        $response->assertJson(['status' => true]);
    } // function test_kegiatan_create_post

    public function test_kegiatan_delete_post(): void
    {
        $kegiatan = KegiatanModel::all()->random()->id;

        $response = $this->post(route('kegiatan.delete_post', ['id' => $kegiatan]));

        $response->assertJson(['status' => true]);
    } // function test_kegiatan_delete_post

    public function test_kegiatan_edit(): void
    {
        $kegiatan = KegiatanModel::all()->random()->id;
        $response = $this->get(route('kegiatan.edit', ['id' => $kegiatan]));
        $response->assertSuccessful();
        $response->assertSee(route('kegiatan.edit_post', ['id' => $kegiatan]));
    } // function test_kegiatan_edit

    public function test_kegiatan_edit_post(): void
    {
        $kegiatan = KegiatanModel::all()->random()->id;
        $response = $this->post(
            route('kegiatan.edit_post', ['id' => $kegiatan]),
            [
                'tanggal' => '20-06-2024',
                'deskripsi' => 'klasfhkasf aslkfhljaskh fjasfh',
                'created_by' => 1,
                'modified_by' => 1,
            ]
        );

        $this->assertDatabaseHas('kegiatan', [
            'tanggal' => '2024-06-20 00:00:00',
            'deskripsi' => 'klasfhkasf aslkfhljaskh fjasfh',
            'created_by' => 1,
            'modified_by' => 1,
        ]);

        $response->assertJson(['status' => true]);
    } // function test_kegiatan_edit_post
}
