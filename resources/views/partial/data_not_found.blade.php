<div class="alert alert-danger d-flex align-items-center" role="alert">
    <div class="d-flex flex-column pe-sm-10 pe-0">
        <h5 class="mb-1">Aduch...</h5>
        <span>Data tidak ditemukan. Coba refresh ulang.</span>
    </div>
</div>
