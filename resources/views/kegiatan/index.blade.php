@extends('layout.admin')

@php
    $langContext = 'views/kegiatan.index.filter';
    $langContextKegiatanIndex = 'views/kegiatan.index.kegiatan-index';
    $options = config('kominfohelper.rows_per_page_options');
    $selected = intval(request()->query('rowsperpage', request()->cookie('rowsperpage', config('kominfohelper.rows_per_page'))));
@endphp

@push('styles')
    <style>
        .calendar {
            cursor: pointer;
        }

        .clicked-date {
            background-color: #51a7f8;
            color: #ffffff;
            font-weight: bold;
        }

        .table-bordered {
            text-align: center;
        }
    </style>
@endPush

@section('content')
    <div class="col-md-12 col-lg-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Kalender</h6>
            </div>
            <div class="card-body">
                {{-- untuk membentuk kalender dinamis --}}
                @php
                    function generateCalendar($year, $month, $pilihan)
                    {
                        $firstDayOfMonth = mktime(0, 0, 0, $month, 1, $year);
                        $daysInMonth = date('t', $firstDayOfMonth);
                        $firstDayOfWeek = date('w', $firstDayOfMonth);

                        $output = '<div class="calendar">';

                        // navigation controls with dropdowns
                        $output .= '<div class="calendar-header">';
                        // pilihan tampil data berdasarkan tahun, bulan, tanggal
                        // $output .= '<select class="form-control mb-3" id="pilihanData" onchange="updateCalendar()">';
                        // foreach (EnumHelper::SELECT_PILIHAN_DATA as $key => $value) {
                        //     $selected = $pilihan == $key ? 'selected' : '';
                        //     $output .= '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
                        // }
                        // untuk pilihan list tahun
                        $output .= '</select>';
                        $output .= '<select class="form-control mb-3" id="yearDropdown" onchange="updateCalendarTahun()">';
                        $currentYear = date('Y');
                        for ($i = 2021; $i <= $currentYear; $i++) {
                            $selected = $i == $year ? 'selected' : '';
                            $output .= '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
                        }
                        // untuk pilihan list bulan
                        $output .= '</select>';
                        $output .= '<select class="form-control mb-3" id="monthDropdown" onchange="updateCalendarBulan()">';
                        for ($i = 1; $i <= 12; $i++) {
                            $selected = $i == $month ? 'selected' : '';
                            $output .= '<option value="' . $i . '" ' . $selected . '>' . date('F', mktime(0, 0, 0, $i, 1, $year)) . '</option>';
                        }
                        $output .= '</select>';
                        $output .= '</div>';

                        $output .= '<div class="table-responsive"><table class="table table-bordered">';
                        $output .= '<thead class="thead-dark">';
                        $output .= '<tr>';
                        $output .= '<th scope="col">M</th><th scope="col">S</th><th scope="col">S</th><th scope="col">R</th><th scope="col">K</th><th scope="col">J</th><th scope="col">S</th>';
                        $output .= '</tr>';
                        $output .= '</thead>';
                        $output .= '<tbody>';
                        $output .= '<tr>';

                        // fill in empty cells for the days before the first day of the month
                        for ($i = 0; $i < $firstDayOfWeek; $i++) {
                            $output .= '<td></td>';
                        }

                        // fill in the days of the month
                        for ($day = 1; $day <= $daysInMonth; $day++) {
                            $output .= '<td>' . $day . '</td>';
                            if (($day + $firstDayOfWeek) % 7 == 0) {
                                $output .= '</tr><tr>';
                            }
                        }

                        // fill in empty cells for the days after the last day of the month
                        $lastDayOfWeek = ($firstDayOfWeek + $daysInMonth) % 7;
                        if ($lastDayOfWeek != 0) {
                            for ($i = $lastDayOfWeek; $i < 7; $i++) {
                                $output .= '<td></td>';
                            }
                        }

                        $output .= '</tr>';
                        $output .= '</tbody>';
                        $output .= '</table>';
                        $output .= '</div>';
                        $output .= '</div>';

                        return $output;
                    }

                    // get the year and month from query parameters or use the current year and month
                    $year = isset($_GET['year']) ? intval($_GET['year']) : date('Y');
                    $month = isset($_GET['month']) ? intval($_GET['month']) : date('n');
                    $pilihan = isset($_GET['pilihan']) ? intval($_GET['pilihan']) : '1';

                    echo generateCalendar($year, $month, $pilihan);
                @endphp
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Daftar Kegiatan</h6>
            </div>
            <div class="card-body card-buttons mt-3 form-inline">
                <a href="javascript:void(0);" class="btn btn-primary" data-mf-id="modalCreate" data-mf-url="{{ route('kegiatan.create', ['clicked_date' => $clickedDate, 'year' => $year, 'month' => $month]) }}" onclick="modalFormShow(this)">Tambah</a>
                {{-- rows per page --}}
                <span class="dropdown mx-2">
                    <button class="btn btn-outline-primary dropdown-toggle" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        {{ $selected }}
                    </button>
                    <div class="dropdown-menu animated--fade-in"
                        aria-labelledby="dropdownMenuButton">
                        @foreach ($options as $option)
                            <a class="dropdown-item {{ $option == $selected ? 'active' : '' }}" href="{{ request()->fullUrlWithQuery(['rowsperpage' => $option]) }}">{{ $option }} baris</a>
                        @endforeach
                    </div>
                </span>
                {{-- info --}}
                <span class="collection-info">
                    {{ $kegiatanList->total() }} data.
                </span>
                <div class="ml-auto">
                    <form action="{{ route('kegiatan.index') }}" method="GET">
                        <input class="form-control" type="text" name="cari" id="cari" value="{{ request()->cari }}" placeholder="kata kunci">
                        <button type="submit" class="btn btn-primary ml-2">Cari</button>
                        @if (request()->cari)
                            <a href="{{ route('kegiatan.index') }}" class="btn btn-secondary ml-2" type="reset" title="reset pencarian">Reset</a>
                        @endif
                    </form>
                </div>
            </div>
            <div class="card-body">
                <div>
                    <table class="table table-index">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>@sortablelink('tanggal', __($langContextKegiatanIndex . '.tanggal'))</th>
                                <th>@sortablelink('deskripsi', __($langContextKegiatanIndex . '.deskripsi'))</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!count($kegiatanList))
                                <tr>
                                    <td colspan="7"><span class="text-muted">Tidak ada data.</span></td>
                                </tr>
                            @else
                                @foreach ($kegiatanList as $kegiatan)
                                    <tr>
                                        <td>{{ $kegiatanList->firstItem() + $loop->index }}</td>
                                        <td>{{ AppHelper::showTanggal($kegiatan->tanggal, 'd F Y') }}</td>
                                        <td>{{ $kegiatan->deskripsi }}</td>
                                        <td class="col-btn2">
                                            <a href="javascript:void(0);" data-mf-id="modalEdit" data-mf-url="{{ route('kegiatan.edit', ['id' => $kegiatan->id]) }}" onclick="modalFormShow(this)" title="{{ __('tooltip.btn_edit') }}"><i class="fas fa-pen"></i></a>
                                            @csrf
                                            <a href="javascript:void(0);" data-deleteurl="{{ route('kegiatan.delete_post', ['id' => $kegiatan->id]) }}" onclick="sweetDeleteData(this)" title="{{ __('tooltip.btn_delete') }}"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                {{ $kegiatanList->links('kominfo-helper::partials.pagination-numbers') }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCreate" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="true" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title fs-5">Tambah Kegiatan</h5>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="true" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title fs-5">Edit Kegiatan</h5>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        // untuk menampilkan data per tanggal
        document.addEventListener('DOMContentLoaded', function() {
            const dateCells = document.querySelectorAll('.calendar td:not(.disabled)');

            dateCells.forEach(function(cell) {
                cell.addEventListener('click', function() {
                    // retrieve the stored clicked date from local storage
                    const storedClickedDate = localStorage.getItem('clickedDate');

                    // check if the clicked date matches the stored clicked date
                    if (this.innerText === storedClickedDate) {
                        // if they match, remove the "clicked-date" class from the clicked cell
                        this.classList.remove('clicked-date');

                        // clear the stored clicked date from local storage
                        localStorage.removeItem('clickedDate');
                    } else {
                        // remove the "clicked-date" class from all date cells
                        dateCells.forEach(function(dateCell) {
                            dateCell.classList.remove('clicked-date');
                        });

                        // add the "clicked-date" class to the clicked date cell
                        this.classList.add('clicked-date');

                        const clickedDate = this.innerText;
                        const currentYear = {{ $year }};
                        const currentMonth = {{ $month }};

                        // store the clicked date in local storage
                        localStorage.setItem('clickedDate', clickedDate);

                        window.location.href = '{{ route('kegiatan.index') }}?clicked_date=' + clickedDate + '&year=' + currentYear + '&month=' + currentMonth;
                    }
                });
            });

            // retrieve the clicked date from local storage and apply the style
            // storedClickedDate dari localStorage
            const storedClickedDate = localStorage.getItem('clickedDate');
            // clickedDateRaw dari backend dan pasti tanggal aktif hari ini kalau belum pernah klik tanggal apapun dari kalender
            const clickedDateRaw = '{{ $clickedDate }}';
            // mengubah format '07' dari backend menjadi '7' biar sesuai dengan kebutuhan logika persamaan
            const clickedDate = String(parseInt(clickedDateRaw, 10));
            const styleDate = storedClickedDate ? storedClickedDate : clickedDate;
            const matchingCell = Array.from(dateCells).find((cell) => cell.innerText === styleDate);
            if (matchingCell) {
                matchingCell.classList.add('clicked-date');
            }
        });




        // // untuk menampilkan data per pilihan combo di card kalender, trigger ketika combo di klik
        function updateCalendarBulan() {
            const yearDropdown = document.getElementById('yearDropdown');
            const monthDropdown = document.getElementById('monthDropdown');
            const selectedYear = yearDropdown.value;
            const selectedMonth = monthDropdown.value;
            window.location.href = `?year=${selectedYear}&month=${selectedMonth}`;
        }

        function updateCalendarTahun() {
            const yearDropdown = document.getElementById('yearDropdown');
            const selectedYear = yearDropdown.value;
            window.location.href = `?year=${selectedYear}`;
        }
    </script>
@endpush
