@php
    $langContext = 'views/kegiatan.edit';
@endphp
<form action="{{ route('kegiatan.edit_post', ['id' => $kegiatan->id]) }}" method="POST">
    @csrf
    <x-form-input :$langContext name="tanggal" old-value="{{ $kegiatan->tanggal }}" placeholder="dd-mm-yyyy"></x-form-input>
    <x-form-textarea :$langContext name="deskripsi" :required="true" old-value='{{ $kegiatan->deskripsi }}' :rows="8"></x-form-textarea>
    <div class="form-buttons">
        {{-- <a href="javascript:void(0);" class="btn btn-outline btn-outline-primary" id="closeButton">Batal</a> --}}
        <a href="javascript:void(0);" class="btn btn-primary" id="submitButton">Simpan</a>
    </div>
</form>
