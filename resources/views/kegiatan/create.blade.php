@php
    $langContext = 'views/kegiatan.create';
@endphp

<form action="{{ route('kegiatan.create_post') }}" method="POST">
    @csrf
    <x-form-input :$langContext name="tanggal" old-value="{{ $tanggalAktif }}" placeholder="dd-mm-yyyy"></x-form-input>
    <x-form-textarea :$langContext name="deskripsi" :required="true" :rows="8"></x-form-textarea>

    <div class="form-buttons">
        {{-- <a href="javascript:void(0);" class="btn btn-outline btn-outline-primary" id="closeButton">Batal</a> --}}
        <a href="javascript:void(0);" class="btn btn-primary" id="submitButton">Tambah</a>
    </div>
</form>
