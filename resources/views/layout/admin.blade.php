<!DOCTYPE html>
<html lang="en">

<head>
    @hasSection('window-title')
        <title>@yield('window-title') | Just Notes</title>
    @else
        <title>Just Notes</title>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('assets/image/favicon.png') }}" />

    <link href="{{ asset('assets/sb-admin-2/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link href="{{ asset('assets/sb-admin-2/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/global.css') }}">

    @stack('styles')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-2">Just Notes</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            @include('layout.adminmenu')

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        {{-- <div class="topbar-divider d-none d-sm-block"></div> --}}

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Admin</span>
                                <img class="img-profile rounded-circle"
                                    src="{{ asset('assets/sb-admin-2/img/undraw_profile.svg') }}">
                            </a>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">
                            @hasSection('page-title')
                                @yield('page-title')
                            @else
                                @hasSection('window-title')
                                    @yield('window-title')
                                @endif
                            @endif
                        </h1>
                        @hasSection('breadcrumb')
                            <!--begin::Breadcrumb-->
                            <ul class="breadcrumb fw-semibold fs-7">
                                @yield('breadcrumb')
                            </ul>
                            <!--end::Breadcrumb-->
                        @endif
                    </div>

                    <!-- Content Row -->
                    <div class="row">
                        @yield('content')
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2023</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script>
        var hostUrl = "assets/";
    </script>

    <script src="{{ asset('assets/sb-admin-2/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <script src="{{ asset('assets/sb-admin-2/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <script src="{{ asset('assets/sb-admin-2/js/sb-admin-2.min.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/global.js') }}"></script>

    <script>
        $(document).ready(function() {
            const sidebarState = localStorage.getItem('sidebarState');
            if (sidebarState === 'hidden') {
                $("#page-top").addClass("sidebar-toggled");
                $("#accordionSidebar").addClass("toggled");
            } else {
                $("#page-top").removeClass("sidebar-toggled");
                $("#accordionSidebar").removeClass("toggled");
            }

            $("#sidebarToggleTop").click(function() {
                if (sidebarState === 'hidden') {
                    localStorage.setItem('sidebarState', 'shown');
                } else {
                    localStorage.setItem('sidebarState', 'hidden');
                }
            });
            $("#sidebarToggle").click(function() {
                if (sidebarState === 'hidden') {
                    localStorage.setItem('sidebarState', 'shown');
                } else {
                    localStorage.setItem('sidebarState', 'hidden');
                }
            });
        });
    </script>

    @stack('scripts')

    @error('error_alert')
        <script>
            $(function() {
                Swal.fire('', '{{ $message }}', 'error');
            });
        </script>
    @enderror
</body>

</html>
