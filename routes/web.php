<?php

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// routine untuk me-load semua file route di folder /routes/web
if (File::exists(__DIR__ . '/web')) {
    foreach (File::allFiles(__DIR__ . '/web') as $route_file) {
        require $route_file->getPathname();
    }
} // if (File::exists())
