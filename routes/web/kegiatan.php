<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KegiatanController;

Route::prefix('/kegiatan')->controller(KegiatanController::class)->group(function () {
    Route::get('/', 'index')->name('kegiatan.index');
    Route::get('/create', 'create')->name('kegiatan.create');
    Route::post('/createpost', 'createPost')->name('kegiatan.create_post');
    Route::get('/edit/{id}', 'edit')->name('kegiatan.edit');
    Route::post('/editpost/{id}', 'editPost')->name('kegiatan.edit_post');
    Route::post('/deletepost/{id}', 'deletePost')->name('kegiatan.delete_post');
});

Route::prefix('/')->controller(KegiatanController::class)->group(function () {
    Route::get('/', 'index')->name('kegiatan.index');
});
