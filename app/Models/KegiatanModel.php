<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use KominfoGusit\LaravelHelper\AppHelper;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class KegiatanModel extends Model
{
    use HasFactory;
    use Sortable;
    protected $table  = 'kegiatan';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected function tanggal(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => AppHelper::showTanggal($value, "d-m-Y"),
            set: fn ($value) => AppHelper::parseTanggal($value, "d-m-Y"),
        );
    }
}
