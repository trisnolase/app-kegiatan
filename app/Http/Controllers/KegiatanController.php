<?php

namespace App\Http\Controllers;

use App\Models\KegiatanModel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Request;
use KominfoGusit\LaravelHelper\AppHelper;
use Illuminate\Support\Facades\Validator;

class KegiatanController extends Controller
{
    public function index(Request $request)
    {
        $clickedDate = $request->input('clicked_date') ?: AppHelper::showTanggal(now(), 'd');
        $year = $request->input('year') ?: AppHelper::showTanggal(now(), 'Y');
        $month = $request->input('month') ?: AppHelper::showTanggal(now(), 'm');
        $tanggalFilter = $year . "-" . $month . "-" . $clickedDate;

        $rowsPerPage = intval(request()->query('rowsperpage', request()->cookie('rowsperpage', config('kominfohelper.rows_per_page'))));
        Cookie::queue('rowsperpage', $rowsPerPage, 360);


        $kegiatanQuery = KegiatanModel::query();
        if ($request->cari) {
            $kegiatanQuery = $kegiatanQuery->where('deskripsi', 'LIKE', '%' . $request->cari . '%');
        } else {
            if ($request->input('month') == null && $request->input('year') == null && $request->input('clicked_date') == null) {
                $kegiatanQuery = $kegiatanQuery->where('tanggal', $tanggalFilter);
            }
            if ($request->input('month') != null && $request->input('year') != null && $request->input('clicked_date') != null) {
                $kegiatanQuery = $kegiatanQuery->where('tanggal', $tanggalFilter);
            }
            if ($request->input('month') != null && $request->input('year') != null && $request->input('clicked_date') == null) {
                $kegiatanQuery = $kegiatanQuery->whereMonth('tanggal', AppHelper::showTanggal($tanggalFilter, 'm'))->whereYear('tanggal', AppHelper::showTanggal($tanggalFilter, 'Y'));
            }
            if ($request->input('month') == null && $request->input('year') != null && $request->input('clicked_date') == null) {
                $kegiatanQuery = $kegiatanQuery->whereYear('tanggal', AppHelper::showTanggal($tanggalFilter, 'Y'));
            }
        }
        $kegiatanList = $kegiatanQuery->sortable(['tanggal' => 'desc'])->paginate($rowsPerPage)->withQueryString();

        return view('kegiatan.index', compact('kegiatanList', 'clickedDate', 'year', 'month'));
    } // function index

    public function create()
    {
        $clickedDate = request()->clicked_date ?: AppHelper::showTanggal(now(), 'd');
        $clickedDate = strlen($clickedDate) == 1 ? '0' . $clickedDate : $clickedDate;
        $month = request()->month ?: AppHelper::showTanggal(now(), 'm');
        $year = request()->year ?: AppHelper::showTanggal(now(), 'Y');
        $bulanData = strlen($month) == 1 ? '-0' : '-';
        $tanggalAktif = $clickedDate . $bulanData . $month . "-" . $year;
        return view('kegiatan.create', compact('tanggalAktif'));
    } // function create

    public function createPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'sometimes|nullable|tglblnthn',
            'deskripsi' => 'required',
        ]);
        if ($validator->fails()) {
            return AppHelper::jsonResponseErrorValidator($validator);
        }

        $kegiatan = new KegiatanModel();
        $kegiatan->tanggal = $request->tanggal ? $request->tanggal :  AppHelper::showTanggal(now(), 'd-m-Y');
        $kegiatan->deskripsi = $request->deskripsi;
        $kegiatan->created_at = now();
        $kegiatan->created_by = 1;
        $kegiatan->modified_at = now();
        $kegiatan->modified_by = 1;

        try {
            $kegiatan->save();
        } catch (\Throwable $th) {
            Log::error('Gagal menambah data kegiatan baru.', ['th' => $th, 'db' => AppHelper::attrToArray($kegiatan)]);
            return AppHelper::jsonResponseErrorTh($th, 'Gagal menambah data kegiatan baru.');
        }

        return AppHelper::jsonResponseSuccess();
    } // function createPost

    public function edit($id)
    {
        $kegiatan = KegiatanModel::find($id);
        if (!$kegiatan) return view('partial.data_not_found');
        return view('kegiatan.edit', compact('kegiatan'));
    } // function edit

    public function editPost($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal' => 'sometimes|nullable|tglblnthn',
            'deskripsi' => 'required',
        ]);
        if ($validator->fails()) {
            return AppHelper::jsonResponseErrorValidator($validator);
        }

        $kegiatan = KegiatanModel::find($id);
        if (!$kegiatan) return AppHelper::jsonResponseErrorMessage(__('error.data_not_found'));
        $kegiatan->tanggal = $request->tanggal ? $request->tanggal :  AppHelper::showTanggal(now(), 'd-m-Y');
        $kegiatan->deskripsi = $request->deskripsi;
        $kegiatan->modified_at = now();
        $kegiatan->modified_by = 1;

        try {
            $kegiatan->update();
        } catch (\Throwable $th) {
            Log::error('Gagal menyimpan data kegiatan', ['th' => $th, 'db' => AppHelper::attrToArray($kegiatan)]);
            return AppHelper::jsonResponseErrorTh($th, 'Gagal menyimpan data kegiatan.');
        }

        return AppHelper::jsonResponseSuccess();
    } // function editPost

    public function deletePost($id)
    {
        $kegiatan = KegiatanModel::find($id);
        if (!$kegiatan) return AppHelper::jsonResponseErrorMessage(__('error.data_not_found'));
        try {
            $lastError = 'Gagal menghapus data kegiatan.';
            $kegiatan->delete();
        } catch (\Throwable $th) {
            if ($th->getCode() == 23000) {
                return AppHelper::jsonResponseErrorMessage(__('error.delete_integrity_violation'));
            } else {
                Log::error($lastError, ['th' => $th, 'db' => AppHelper::attrToArray($kegiatan)]);
                return AppHelper::jsonResponseErrorTh($th, $lastError);
            }
        }
        return AppHelper::jsonResponseSuccess();
    } // function deletePost

} // class KegiatanController
