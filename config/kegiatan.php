<?php

return [
    'fileupload_input_accept' => env('KEGIATAN_FILEUPLOAD_INPUT_ACCEPT', '.pdf,.jpg,.jpeg,.png'),
    'fileupload_allowed_extension' =>  explode(",", env('KEGIATAN_FILEUPLOAD_ALLOWED_EXTENSION', 'pdf,jpg,jpeg,png')),
    'fileupload_allowed_extension_human' =>  env('KEGIATAN_FILEUPLOAD_ALLOWED_EXTENSION_HUMAN', 'PDF, JPG, JPEG, PNG'),
    'fileupload_max_size' =>  env('KEGIATAN_FILEUPLOAD_MAX_SIZE', 1000000),
    'fileupload_max_size_human' =>  env('KEGIATAN_FILEUPLOAD_MAX_SIZE_HUMAN', "1 MB"),
];
