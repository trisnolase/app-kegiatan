<?php

return [

    // kamus label untuk menampilkan data dari database

    // ------------------------------------------------------------
    // contoh:
    // 
    // 'kegiatan.edit.nama' => 'Nama Lengkap',
    // 'kegiatan.edit.nama_help' => 'Tuliskan nama lengkap kegiatan',
    // 
    // maka lang-context pada component: db.kegiatan.edit
    // ------------------------------------------------------------


    'index.filter.tanggal' => 'Tanggal',
    'index.filter.cari' => 'Cari',

    'index.kegiatan-index.tanggal' => 'Tanggal',
    'index.kegiatan-index.deskripsi' => 'Deskripsi',

    'create.tanggal' => 'Tanggal',
    'create.deskripsi' => 'Deskripsi',

    'edit.tanggal' => 'Tanggal',
    'edit.deskripsi' => 'Deskripsi',

];
