<?php

return [

    /* ------------------ pesan error standar, jangan disentuh ------------------ */
    'internal_error' => 'Ada kesalahan internal sistem.',
    'data_not_found' => 'Data tidak ditemukan.',
    'data_not_valid' => 'Data yang diterima tidak valid.',
    'operation_invalid' => 'Operasi tidak valid.',
    'operation_failed' => 'Operasi gagal.',
    'db_failed' => 'Operasi database gagal.',
    'access_denied' => 'Tidak memiliki akses.',
    'file_too_big' => 'Ukuran file terlalu besar.',
    'file_type_not_allowed' => 'Tipe file tidak didukung.',
    'file_type_not_allowed_ex' => 'Tipe file :tipe tidak didukung.',
    'delete_integrity_violation' => 'Data tidak dapat dihapus karena terkait dengan data lain.',

    /* --------------- pesan error kustom, tambahkan di bawah ini --------------- */

];
